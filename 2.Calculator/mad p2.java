package com.example.calculator_1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button button1 , button2 , button3 , button4 , button5 , button6 , button7 , button8 , button9 , button0 ;
    Button buttonadd , buttonsub , buttonmul , buttondiv ;
    Button buttonclear , buttonequal , buttondot ;
    EditText txtresult ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button1 = (Button) findViewById(R.id.but1);
        button1.setOnClickListener(this);

        button2 = (Button) findViewById(R.id.but2);
        button2.setOnClickListener(this);

        button3 = (Button) findViewById(R.id.but3);
        button3.setOnClickListener(this);

        button4 = (Button) findViewById(R.id.but4);
        button4.setOnClickListener(this);

        button5 = (Button) findViewById(R.id.but5);
        button5.setOnClickListener(this);

        button6 = (Button) findViewById(R.id.but6);
        button6.setOnClickListener(this);

        button7 = (Button) findViewById(R.id.but7);
        button7.setOnClickListener(this);

        button8 = (Button) findViewById(R.id.but8);
        button8.setOnClickListener(this);

        button9 = (Button) findViewById(R.id.but9);
        button9.setOnClickListener(this);

        button0 = (Button) findViewById(R.id.but0);
        button0.setOnClickListener(this);

        buttonadd = (Button) findViewById(R.id.butplus);
        buttonadd.setOnClickListener(this);

        buttonmul = (Button) findViewById(R.id.butmul);
        buttonmul.setOnClickListener(this);

        buttonsub = (Button) findViewById(R.id.butminus);
        buttonsub.setOnClickListener(this);

        buttondiv = (Button) findViewById(R.id.butdiv);
        buttondiv.setOnClickListener(this);

        buttondot = (Button) findViewById(R.id.butdot);
        buttondot.setOnClickListener(this);

        buttonequal = (Button) findViewById(R.id.butequals);
        buttonequal.setOnClickListener(this);
        
        buttonclear = (Button) findViewById(R.id.butclear);
        buttonclear.setOnClickListener(this);
        
        txtresult = (EditText) findViewById(R.id.calresult);
        txtresult.setText("");
    }

    @Override
    public void onClick(View v) {
        if(v.equals(button0))
            txtresult.append("0");
        if(v.equals(button1))
            txtresult.append("1");
        if(v.equals(button2))
            txtresult.append("2");
        if(v.equals(button3))
            txtresult.append("3");
        if(v.equals(button4))
            txtresult.append("4");
        if(v.equals(button5))
            txtresult.append("5");
        if(v.equals(button6))
            txtresult.append("6");
        if(v.equals(button7))
            txtresult.append("7");
        if(v.equals(button8))
            txtresult.append("8");
        if(v.equals(button9))
            txtresult.append("9");
        if(v.equals(buttonadd))
            txtresult.append("+");
        if(v.equals(buttonsub))
            txtresult.append("-");
        if(v.equals(buttonmul))
            txtresult.append("*");
        if(v.equals(buttondiv))
            txtresult.append("/");
        if(v.equals(buttondot))
            txtresult.append(".");
        if(v.equals(buttonclear))
            txtresult.setText("");
        if(v.equals(buttonequal))
        {
            try 
            {
                String data = txtresult.getText().toString();
                if(data.contains("/"))
                {
                    divison(data);
                }
                else if(data.contains("*"))
                {
                    multiplication(data);
                }
                else if(data.contains("+"))
                {
                    addition(data);
                }
                else if(data.contains("-"))
                {
                    subtraction(data);
                }
            } 
            catch (Exception e) {
                displayInvalidMessage("invalid operator\n");
            }
        }

    }

    private void displayInvalidMessage(String msg) {
        Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
    }

    private void subtraction(String data) {
        String[] operands = data.split("-");
        if(operands.length==2)
        {
            double operand1 = Double.parseDouble(operands[0]);
            double operand2 = Double.parseDouble(operands[1]);
            double result = operand1-operand2 ;
            txtresult.setText(String.valueOf(result));
        }
        else
        {
            displayInvalidMessage("invalid input");
        }

    }

    private void addition(String data) {
        String[] operands = data.split(Pattern.quote("+"));
        if(operands.length==2)
        {
            double operand1 = Double.parseDouble(operands[0]);
            double operand2 = Double.parseDouble(operands[1]);
            double result = operand1+operand2;
            txtresult.setText(String.valueOf(result));
        }
        else
        {
            displayInvalidMessage("invalid input");
        }
    }

    private void multiplication(String data) {
        String[] operands = data.split(Pattern.quote("*"));
        if(operands.length==2)
        {
            double operand1 = Double.parseDouble(operands[0]);
            double operand2 = Double.parseDouble(operands[1]);
            double result = operand1 * operand2 ;
            txtresult.setText(String.valueOf(result));
        }
        else
        {
            displayInvalidMessage("invalid input");
        }
    }

    private void divison(String data) {
        String[] operands = data.split("/");
        if(operands.length==2)
        {
            double operand1 = Double.parseDouble(operands[0]);
            double operand2 = Double.parseDouble(operands[1]);
            double result = operand1 / operand2 ;
            txtresult.setText(String.valueOf(result));
        }
        else
        {
            displayInvalidMessage("invalid input");
        }
    }
}